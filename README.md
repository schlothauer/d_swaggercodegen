Simple image wraps the swagger code generation 


Version: 0.1


To start a container from that image call `./bin/start.sh`

```bash
# example call to run swagger-codegen
bin/run_swaggercodegen.sh -i ~/somewhere/test.swagger.yaml -o /tmp/swagger-test -l jaxrs --api-package=de.test.server --group-id=de.test --artifact-id=test-server --model-package=de.test.model -DhideGenerationTimestamp=true
```



