#!/bin/bash
scriptPos=${0%/*}

source "$scriptPos/image_conf.sh"

function show_help() {
echo '''
Swagger code generation tools allow to generate source code from swagger files.
Swagger files are definitions of "REST" APIs

The script takes the same commandline arguments as swagger-codegen-cli.jar. It simply pass it to
the swagger Java process.

example usage:
./run_swaggercodegen.sh generate -l jaxrs -i MY_SWAGGER_FILE.yaml -o tmp/server \
    --api-package=de.test.server --group-id=de.test --artifact-id=test-server \
    --model-package=de.test.model -DhideGenerationTimestamp=true
'''
}

function checkInputAndExitIfEmpty() {
    variableToCheck=$1
    msgTxt=$2
    if [ -z "$variableToCheck" ]; then
        echo "$msgTxt"
        echo
        show_help
        exit 1
    fi
}

#    @Parameter(names = [ '-i', '--swagger-file' ], description = "Path to the swagger file to parse", required = true)
#    @Parameter(names = [ '-o', '--outputBase' ], description = "Base directory for the output", required = true)
#    @Parameter(names = ['-h','--help'], help = true)


while true; do
    case $1 in
        -h|\?)
            show_help
            exit 0
            ;;
    -i)  shift
        model=$1
        shift
        ;;
    -o)  shift
        outputBase=$1
        shift
        ;;
    -*) extraParameters+=" $1 "
        shift
        extraParameters+="$1"
        shift
        ;;
    esac
    if [ -z "$1" ]; then break; fi
done

checkInputAndExitIfEmpty "$model" "swagger file is needed"
checkInputAndExitIfEmpty "$outputBase" "the directory to write the output is needed"

if ! [ -f "$model" ]; then
    echo
    echo "given swagger file not found in file system: $model"
    exit 1
fi

if ! [ -d "$outputBase" ]; then
    echo
    echo "disired output directory doesn't exist: $outputBase"
    exit 1
fi

modelDir=${model%/*}
absolutModelDir=`pushd "$modelDir" > /dev/null;pwd;popd > /dev/null`
modelFile=${model##*/}

outputDir=`pushd "$outputBase" > /dev/null;pwd;popd > /dev/null`

aktImgName=`docker images |  grep -G "$imageBase *$imageTag *" | awk '{print $1}'`
aktImgVers=`docker images |  grep -G "$imageBase *$imageTag *" | awk '{print $2}'`


if [ "$aktImgName" == "$imageBase" ] && [ "$aktImgVers" == "$imageTag" ]
then
        echo "run container from image: $aktImgName:$aktImgVers"
else
    if docker build -t $imageName $scriptPos/../image
    then
        echo -en "\033[1;34m  image created: $1 \033[0m\n"
    else
        echo -en "\033[1;31m  error while create image: $imageName \033[0m\n"
        exit 1
    fi
fi

echo "extraParameters: $extraParameters"

docker run --rm -it -v "$absolutModelDir":/opt/model -v "$outputDir":/opt/output "$imageName" /opt/run_swagger.sh -o "/opt/output" -i "/opt/model/$modelFile" $extraParameters


